# Summary #

Made in 3 hours for the Alameda County 2014.1 Hackathon, Civic Reports was made in collaboration with [Jimmy Hsu](https://github.com/jimmyhsu). We quickly designed and drafted a mockup for a website that would use the County of Alameda's data to plot points on a Google Map of locations of crimes. Users could also add their own points to the map and actually report crime, if they had an active internet connection.

Working quickly was an awesome challenge, and made me really glad to be learning about and using such useful tools as Bower and Grunt!